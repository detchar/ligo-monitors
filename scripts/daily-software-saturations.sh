#!/bin/bash
#
# Run the gwdetchar.saturation code on a daily stride

export MPLCONFIGDIR=${HOME}/.matplotlib
. ~/.bash_profile
if [[ "$(whoami)" == "detchar" ]]; then
    unset X509_USER_PROXY
fi
set -e

# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# set python environment
activate_conda_environment
echo "-- Environment set"
echo "Conda prefix: ${CONDA_PREFIX}"

# get run date
if [ -z "$1" ]; then
    # get yesterday date in case no arguments is given
    gpsstart=`gps_start_yesterday`
    gpsend=`gps_start_today`
else
    gpsstart=`gps_start_date ${1}`
    gpsend=`gps_end_date ${1}`
fi

date_=`yyyymmdd ${gpsstart}`
duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/software-saturations/
outdir=${htmlbase}/day/${date_}
mkdir -p ${outdir}
cd ${outdir}
echo "-- Moved to output directory: `pwd`"

# set up channels to skip (funky bash to allow adding skips as newlines)
skip=`echo "
ALS-
PSL-ISS_INLOOP_PD_SELECT_CALI
CAL-CS_DARM_FE_ETMY_M0_LOCK_L
ISI-GND_BRS_ETMX_RY
" | tr '\n' ' '`

if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi

# get analysis flag
FLAG="${IFO}:DMT-GRD_ISC_LOCK_NOMINAL:1"

# run the code
nagios_status 0 "Daily overflows analysis for ${date_} is running" 10000 "Daily overflows analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json
set +e

cmd="python -m gwdetchar.saturation $gpsstart $gpsend --ifo ${IFO} --frametype ${IFO}_R --html index.html --plot --nproc 4 --state-flag ${FLAG} --pad-state-end 1 --skip ${skip}"
echo "$ $cmd" && eval $cmd 2> /tmp/daily-software-saturations-$(whoami).err

EXITCODE="$?"

# write JSON output
TIMER=100000
TIMEOUT="Daily software saturations analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily software saturations analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily software saturations analysis for ${date_} failed with exitcode $EXITCODE" && sed ':a;N;$!ba;s/\n/\\\n/g' /tmp/daily-software-saturations-$USER.err | tr '"' "'"`
    nexit=2
fi

nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
